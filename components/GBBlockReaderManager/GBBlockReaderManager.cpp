/*
 * GBBlockReaderManager.cpp
 *
 *  Created on: Jun 23, 2018
 *      Author: KING
 */


#include <GBBlockReaderManager.h>
#include <MFRC522.h>
#include <esp_log.h>
#include <vector>
static const char *TAG = "rfid" ;
byte startBlock[]    = {
    0x6b, 0x6f, 0x64, 0x69, //  1,  2,   3,  4, //kodimo
    0x00, 0x00, 0x00, 0x00, //  5,  6,   7,  8,
	0x00, 0x00, 0x00, 0x00, //  9, 10, 255, 12,
	0x00, 0x00, 0x00, 0x00  // 13, 14,  15, 16
};
byte endBlock[]    = {
    0x6b, 0x6f, 0x64, 0x69, //  1,  2,   3,  4,
    0xff, 0xff, 0xff, 0xff, //  5,  6,   7,  8,
	0xff, 0xff, 0xff, 0xff, //  9, 10, 255, 12,
	0xff, 0xff, 0xff, 0xff  // 13, 14,  15, 16
};
byte runBlock[]    = {
    0x6b, 0x6f, 0x64, 0x69, //  1,  2,   3,  4,
    0x00, 0x00, 0xff, 0xff, //  5,  6,   7,  8,
	0xff, 0xff, 0xff, 0xff, //  9, 10, 255, 12,
	0xff, 0xff, 0xff, 0xff  // 13, 14,  15, 16
};
MFRC522 mfrc;
GBBlockReaderManager::GBBlockReaderManager() {


	// TODO Auto-generated destructor stub

}

GBBlockReaderManager::~GBBlockReaderManager() {
	// TODO Auto-generated destructor stub
}
void GBBlockReaderManager::MFRC_init()
{
	mfrc.PCD_Init(SC_PIN,RST_PIN);
	ESP_LOGI(TAG,"Done init mfrc");
	mfrc.PCD_DumpVersionToSerial();
	modeRead = ALONE ; //single raed block
}

// bufferAddr : address is address will save data was read
void GBBlockReaderManager::MFRC_readDataofBlockNumber(byte BlockNum, byte *bufferAddr)
{
	byte buffer[18];
	byte bufferSize = sizeof(buffer);
	MFRC522::MIFARE_Key key;
	MFRC522::StatusCode status;
	for (byte i = 0 ; i < 6 ; i++ ){
		key.keyByte[i]=0xFF;
	}
	if (!mfrc.PICC_IsNewCardPresent()){ return; }
	if (!mfrc.PICC_ReadCardSerial()) { return; }
	mfrc.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A,BlockNum,&key,&(mfrc.uid));

	status = (MFRC522::StatusCode) mfrc.MIFARE_Read(BlockNum,buffer,&bufferSize);
	if (status != MFRC522::STATUS_OK )
	{
		ESP_LOGI(TAG,"Authencation failed");
	}
	//is kodimo block
	for(byte i = 0; i < 16 ; i++)
	{
		bufferAddr[i]=buffer[i];
	}
	mfrc.PICC_HaltA();
	mfrc.PCD_StopCrypto1();

}
//block looking
bool GBBlockReaderManager::isKodimoBlock(byte *bufferAddr){
	if (	bufferAddr[0]!='k'&&\
			bufferAddr[1]!='o'&&\
			bufferAddr[2]!='d'&&\
			bufferAddr[3]!='i')
		return false;
	return true;
}
bool GBBlockReaderManager::isStartBlock(byte *bufferAddr){

	if (compareBlock(bufferAddr,startBlock,16)) return true;
	return false;
}
bool GBBlockReaderManager::isEndBlock(byte *bufferAddr){

	if (compareBlock(bufferAddr,endBlock,16)) return true;
	return false;
}
bool GBBlockReaderManager::isRunBlock(byte *bufferAddr){

	if (compareBlock(bufferAddr,runBlock,16)) return true;
	return false;
}
//write data in block---------------------------------------
void GBBlockReaderManager::MFRC_writeDataofBockNumber(byte BlockNum, byte *bufferAddr)
{
		MFRC522::MIFARE_Key key;
		MFRC522::StatusCode status;
		for (byte i = 0 ; i < 6 ; i++ ){
			key.keyByte[i]=0xff;
		}
		if (!mfrc.PICC_IsNewCardPresent()){ return; }
		if (!mfrc.PICC_ReadCardSerial()) { return; }
		mfrc.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A,BlockNum,&key,&(mfrc.uid));

	    status = (MFRC522::StatusCode) mfrc.MIFARE_Write(BlockNum, bufferAddr, 16);
		if (status != MFRC522::STATUS_OK )
		{
			ESP_LOGI(TAG,"Authencation failed");
		} else {
			ESP_LOGI(TAG,"Authencation OK");
		}
		mfrc.PICC_HaltA();
		mfrc.PCD_StopCrypto1();


}
bool GBBlockReaderManager::compareBlock(byte *bufferAddr1,byte *bufferAddr2,byte bufferSize){
	for(byte i = 0; i < bufferSize ; i++)
	{
		if(bufferAddr1[i]!=bufferAddr2[i]) return false;
	}
	return true;
}

