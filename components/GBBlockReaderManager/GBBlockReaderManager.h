/*
 * GBBlockReaderManager.h
 *
 *  Created on: Jun 23, 2018
 *      Author: KING3006
 */

#ifndef GBBLOCKREADERMANAGER_H_
#define GBBLOCKREADERMANAGER_H_

#include <string>
#include <vector>
#include <MFRC522.h>
#define SC_PIN 15
#define RST_PIN 0
#define BLOCK_TO_READ 1
using namespace std;


// define block

class GBBlockReaderManager{
public:
	enum modeRead {
		ALONE ,
		MULTI
	} modeRead ;
	struct Block
	{
		byte block[4];
	};
	vector <Block> vBlock;
	Block BlockToPush;
	GBBlockReaderManager();

	virtual ~GBBlockReaderManager();



	void MFRC_init(); //init
	void MFRC_readDataofBlockNumber(byte BlockNum, byte *bufferAddr);
	void MFRC_writeDataofBockNumber(byte BlockNum, byte *bufferAddr);
	void MFRC_nameBlock(byte *bufferAddr);// sau nay viet ham nay
	bool isKodimoBlock(byte *bufferAddr);
	bool isStartBlock(byte *bufferAddr);
	bool isEndBlock(byte *bufferAddr);
	bool isRunBlock(byte *bufferAddr);
	bool compareBlock(byte *bufferAddr1,byte *bufferAddr2,byte len);
	void transmitvBlock();
	void run();
private:



};



#endif /* GBBLOCKREADERMANAGER_H_ */
