#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <esp_log.h>
#include <GBBlockReaderManager.h>
#include <vector>
#include "driver/gpio.h"

#define BLINK_GPIO GPIO_NUM_2
typedef uint8_t byte;
static const char *TAG = "rfid" ;



GBBlockReaderManager readerBlock;



extern "C" {void app_main();}
void blink_led();
void dump_buffer (byte *buffer,byte bufferSize);
void dump_block(std::vector<GBBlockReaderManager::Block> vBlock);
void clear_buffer(byte *buffer);
static void rfid_task(void *param)
{
	byte bufferAddr[16]	;

	readerBlock.MFRC_init();

while(1)
	{
		readerBlock.MFRC_readDataofBlockNumber(BLOCK_TO_READ,bufferAddr);
		if(readerBlock.isKodimoBlock(bufferAddr)){
			blink_led();
			ESP_LOGI(TAG,"isKodimoBlock");

			for(byte i = 0 ; i < 3 ;i ++){
				readerBlock.BlockToPush.block[i]=bufferAddr[i+4];
				}
			if(readerBlock.isStartBlock(bufferAddr)) {
				//delete old vector
				readerBlock.vBlock.clear();
				//create new vector
				readerBlock.vBlock.push_back(readerBlock.BlockToPush);
				ESP_LOGI(TAG,"isStartBlock");

			}
			else if (readerBlock.isEndBlock(bufferAddr)){
				//addblock
				readerBlock.vBlock.push_back(readerBlock.BlockToPush);
				//transmit data
//				readerBlock.transmitvBlock();
				//callback waiting....
				ESP_LOGI(TAG,"isEndBlock");
			}
			else if(!readerBlock.vBlock.empty()){				//have a vector?
				//add block into vector
				ESP_LOGI(TAG,"ortherBlock");
				readerBlock.vBlock.push_back(readerBlock.BlockToPush);

			} else if(readerBlock.isRunBlock(bufferAddr)){
//				readerBlock.run();
			}
			ESP_LOGI(TAG,"size vBlock = %d",readerBlock.vBlock.size());
//			dump_block(readerBlock.vBlock);
		}
		clear_buffer(bufferAddr);


	}
}
void dump_buffer(byte *buffer, byte bufferSize ){
	for (byte i = 0; i <bufferSize; i++){
		ESP_LOGI(TAG,"%x " ,buffer[i]);
	}
}
void dump_block(std::vector<GBBlockReaderManager::Block> vBlock){
//	vector<GBBlockReaderManager::Block>::iterator it=vBlock->begin() ;
	for (byte i = 0; i < vBlock.size(); i++){
		ESP_LOGI(TAG,"vBlock[%d]=%x ,%x , %x, %x \n\r"\
				,i,vBlock[i].block[0],vBlock[i].block[1]\
				,vBlock[i].block[2],vBlock[i].block[3]);
	}
}
void clear_buffer(byte *buffer ){
	for (byte i = 0; i <sizeof(buffer); i++){
		buffer[i]= 0x00;
	}
}
void blink_led()
{
    gpio_pad_select_gpio(BLINK_GPIO);
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
    gpio_set_level(BLINK_GPIO, 0);
    vTaskDelay(200 / portTICK_PERIOD_MS);
    gpio_set_level(BLINK_GPIO, 1);
    vTaskDelay(200 / portTICK_PERIOD_MS);

}
void app_main()
{
	xTaskCreate(&rfid_task, "rfid_task", 4096, NULL, 1, NULL);

};
